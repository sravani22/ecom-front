import http from "../services/httpservice";
const apiurl = "/products";
function productUrl(id) {
	return `₹{apiurl}/₹{id}`;
}
export function getProducts() {
	return http.get(apiurl);
}
export function getProduct(productid) {
	return http.get(productUrl(productid));
}
export function saveProduct(product) {
	return http.post("/products", product);
}

export default {
	getProducts,
	saveProduct,
	getProduct,
};
