import React, { Component } from "react";
// import { Link } from "react-router-dom";
import ReactImageZoom from "react-image-zoom";
import Header from "../common/header";
import cart from "../../services/cartService";
import { toast } from "react-toastify";
import { connect } from "react-redux";
import cart_list from "../../redux/actions/cartlistActions";
import ReactImageMagnify from "react-image-magnify";

class ProductDetails1 extends Component {
	state = {
		product: {},
		image: "",
		quantity: "1",
	};
	async componentDidMount() {
		// await this.setState({ product: this.props.history.location.state.data });
		

		await this.setState({ image: this.state.product.image });
	}
	//   handlechange = async (evt) => {
	//     await this.setState({ [evt.target.name]: evt.target.value });
	//   };
	//   image = async (item) => {
	//     await this.setState({ image: item });
	//   };
	//   addcart = async () => {
	//     toast.configure();
	//     const obj = {
	//       itemid: this.state.product.itemid,
	//       size: "20",
	//       qty: parseInt(this.state.quantity),
	//     };
	//     try {
	//       const data = await cart.addcart(obj);
	//       console.log(data);
	//       if (data.data.success) {
	//         await cart_list();
	//         toast.success(data.data.success, {
	//           position: toast.POSITION.BOTTOM_LEFT,
	//         });
	//       }
	//     } catch (ex) {
	//       if (ex.response && ex.response.status === 400) {
	//         toast.error(ex.response.data, {
	//           position: toast.POSITION.BOTTOM_LEFT,
	//         });
	//       }
	//     }
	//   };
	render() {
		const { product } = this.state;

		return (
			<React.Fragment>
				<Header history={this.props.history} />
				<div id="shopify-section-product-template" class="shopify-section">
					<div class="container container--flush">
						<div class="page_sub-header">
							<nav aria-label="Breadcrumb" class="breadcrumb">
								{/* <ol class="breadcrumb_list">
                  <li class="breadcrumb_item">
                    <a
                      class="breadcrumb_link link"
                      href="http://saiproj.000webhostapp.com/"
                    >
                      {" "}
                      Home{" "}
                    </a>{" "}
                    <i class="fas fa-angle-right"> </i>{" "}
                  </li>
                  <li class="breadcrumb_item">
                    <a
                      class="breadcrumb_link link"
                      href="http://saiproj.000webhostapp.com/ware/product.html"
                    >
                      {" "}
                      Sales - Home{" "}
                    </a>{" "}
                    <i class="fas fa-angle-right"> </i>
                  </li>
                  <li class="breadcrumb_item">
                    {" "}
                    <span class="breadcrumb_link" aria-current="page">
                      {" "}
                      Sony XBR-950G BRAVIA 4K HDR Ultra HD TV{" "}
                    </span>
                  </li>
                </ol> */}
							</nav>
						</div>
					</div>

					<div class="container">
						<div class="card">
							<div class="sidenav">
								<img
									src="https://rukminim1.flixcart.com/image/416/416/k5lcvbk0/computer/4/r/s/dell-na-laptop-original-imafz8txb9fyrhs3.jpeg?q=70"
									class="w-100"
								/>
							</div>

							<div class="product-sticky">
								<div class="card_section">
									<div class="product-meta">
										<h1 class="product-meta_title heading h1">
											Sony XBR-950G BRAVIA 4K HDR Ultra HD TV
										</h1>
										<div class="product-meta_label-list">
											<span class="product-label product-label--on-sale">
												Save <span>₹800</span>
											</span>
										</div>
										<div class="product-meta_reference">
											<a
												class="product-meta_vendor link link--accented"
												href="http://saiproj.000webhostapp.com/ware/product.html"
											>
												Sony
											</a>
											<span class="product-meta_sku">
												SKU: SON-695219-XBR-5{" "}
											</span>
										</div>
									</div>

									<hr class="card_separator" />
									<form
										method="post"
										action="http://saiproj.000webhostapp.com/ware/product.html"
										class="product-form"
									>
										<div class="product-form_variants">
											<div class="product-form_option">
												<span class="product-form_option-name text--strong">
													Size:
												</span>

												<div class="block-swatch-list">
													<div class="block-swatch">
														<input
															class="block-swatch_radio product-form_single-selector"
															type="radio"
															value=""
														/>
														<label
															class="block-swatch_item"
															for="product-template-1916221128755-1-1"
															title='55"'
														>
															<span class="block-swatch_item-text">55"</span>
														</label>
													</div>
													<div class="block-swatch">
														<input
															class="block-swatch_radio product-form_single-selector"
															type="radio"
															name="product-template-1916221128755-1"
														/>
														<label
															class="block-swatch_item"
															for="product-template-1916221128755-1-2"
															title='65"'
														>
															<span class="block-swatch_item-text">65"</span>
														</label>
													</div>
												</div>
											</div>
										</div>

										<div class="product-form_info-list">
											<div class="product-form_info-item">
												<span class="product-form_info-title text--strong">
													Price:
												</span>

												<div class="product-form_info-content">
													<div class="price-list">
														<span class="price price--highlight">₹1,398</span>
														<span class="price price--compare">₹2,198</span>
													</div>
												</div>
											</div>
											<div class="product-form_info-item">
												<span class="product-form_info-title text--strong">
													Stock:
												</span>

												<div class="product-form_info-content">
													<span class="product-form_inventory inventory inventory--high">
														In stock{" "}
													</span>
												</div>
											</div>

											<div class="product-form_info-item product-form_info-item--quantity">
												<label
													for="product-template-1916221128755-quantity"
													class="product-form_info-title text--strong"
												>
													Quantity:
												</label>

												<div class="product-form_info-content">
													<div class="select-wrapper select-wrapper--small select-wrapper--primary">
														<select
															name="quantity"
															class="product-form_quantity"
														>
															<option value="1" selected="selected">
																1
															</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="10">10+</option>
														</select>
													</div>
												</div>
											</div>
										</div>

										<div class="product-form_payment-container">
											<button
												type="submit"
												class="product-form_add-button button button--primary"
											>
												Add to cart
											</button>
										</div>
									</form>
								</div>

								<div class="card__header pt-0">
									<h2 class="card__title heading h3 "> Available offers </h2>
									<hr class="card_separator mb-0 " />
								</div>

								<div class="card__section expandable-content">
									<div class="rte text--pull">
										<ul>
											<li>
												{" "}
												Bank Offer5% Unlimited Cashback on Flipkart Axis Bank
												Credit Card (T&amp;C){" "}
											</li>
											<li>
												{" "}
												Bank Offer10% off* with Axis Bank Buzz Credit Card
												T&amp;C{" "}
											</li>
											<li>
												{" "}
												No cost EMI ₹2,291/month. Standard EMI also available
												&nbsp;{" "}
												<a href="http://saiproj.000webhostapp.com/ware/product.html">
													{" "}
													View Plans <i class="fa fa-angle-right"></i>{" "}
												</a>{" "}
											</li>
											<li>
												{" "}
												Get upto ₹8400 off on exchange &nbsp;{" "}
												<a href="http://saiproj.000webhostapp.com/ware/product.html">
													{" "}
													Buy with Exchange <i class="fa fa-angle-right"></i>{" "}
												</a>{" "}
											</li>
										</ul>
									</div>
								</div>

								<div class="card__header pt-0">
									<h2 class="card__title heading h3 "> Highlights </h2>
									<hr class="card_separator mb-0 " />
								</div>

								<div class="card__section expandable-content">
									<div class="rte text--pull">
										<ul>
											<li> 15.6 inch HD LED Backlit Truelife Display </li>
											<li>
												Once your product has shipped, it usually takes 2 to 3
												business days in USA, 3 to 8 in Europe.
											</li>
											<li>
												You can return your product up to 14 days after
												receiving your order.{" "}
											</li>
										</ul>
									</div>
								</div>

								<div class="card__header pt-0">
									<h2 class="card__title heading h3 "> Specifications </h2>
									<hr class="card_separator mb-0 " />
								</div>

								<table class="table mb-3">
									<tbody>
										<tr class="border-t-0">
											<td class=" pb-0">
												<span> Sales Package </span>
											</td>
											<td class="line-item__title link text--strong  pb-0">
												{" "}
												Laptop, Power Adaptor, User Guide, Warranty Documents{" "}
											</td>
										</tr>
										<tr class="border-t-0">
											<td class=" pb-0">
												<span> Model Number </span>
											</td>
											<td class="line-item__title link text--strong  pb-0">
												{" "}
												vos / vostro 3581{" "}
											</td>
										</tr>

										<tr class="border-t-0">
											<td class=" pb-0">
												<span> Sales Package </span>
											</td>
											<td class="line-item__title link text--strong  pb-0">
												{" "}
												Laptop, Power Adaptor, User Guide, Warranty Documents{" "}
											</td>
										</tr>
										<tr class="border-t-0">
											<td class=" pb-0">
												<span> Model Number </span>
											</td>
											<td class="line-item__title link text--strong  pb-0">
												{" "}
												vos / vostro 3581{" "}
											</td>
										</tr>

										<tr class="border-t-0">
											<td class=" pb-0">
												<span> Sales Package </span>
											</td>
											<td class="line-item__title link text--strong  pb-0">
												{" "}
												Laptop, Power Adaptor, User Guide, Warranty Documents{" "}
											</td>
										</tr>
										<tr class="border-t-0">
											<td class=" pb-0">
												<span> Model Number </span>
											</td>
											<td class="line-item__title link text--strong  pb-0">
												{" "}
												vos / vostro 3581{" "}
											</td>
										</tr>
									</tbody>
								</table>

								<div class="card__header pt-0">
									<h2 class="card__title heading h3 ">
										{" "}
										Ratings &amp; Reviews{" "}
									</h2>
									<hr class="card_separator mb-0 " />
								</div>

								<div class="card__section expandable-content">
									<div class="rte text--pull">
										<h4>
											{" "}
											<span class="rating-badge-normal">
												{" "}
												4.5 <i class="fa fa-star"> </i>{" "}
											</span>{" "}
											Best choice in under 27000 for 15.6 inch catagory{" "}
										</h4>
										<p>
											{" "}
											It's very beautifully and elegantly designed laptop that I
											have got till now. CPU vents are put behind the laptop.
											Battery is not removable and encased inside the bottom
											panel. Overall it's very neatly build into a very slim
											formfactor. Display is average since it's HD. touchpad is
											average.
										</p>
									</div>
								</div>

								<div class="card__section expandable-content">
									<div class="rte text--pull">
										<h4>
											{" "}
											<span class="rating-badge-low">
												{" "}
												2.0 <i class="fa fa-star"> </i>{" "}
											</span>{" "}
											Too Bad Product{" "}
										</h4>
										<p>
											{" "}
											It's very beautifully and elegantly designed laptop that I
											have got till now. CPU vents are put behind the laptop.
											Battery is not removable and encased inside the bottom
											panel. Overall it's very neatly build into a very slim
											formfactor. Display is average since it's HD. touchpad is
											average.
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="shopify-section">
						<section
							class="section"
							data-section-type="featured-collection"
							data-section-settings="{ }"
						>
							<div class="container">
								<header class="section_header">
									<div class="section_header-stack">
										<h2 class="section_title heading h3">
											{" "}
											You may also like{" "}
										</h2>
									</div>
									<a
										href="http://saiproj.000webhostapp.com/ware/collections/sales.html"
										class="section_action-link link"
									>
										View all sales
										<svg
											class="icon icon--tail-right"
											viewBox="0 0 24 24"
											role="presentation"
										>
											<path
												fill="currentColor"
												d="M22.707 11.293L15 3.586 13.586 5l6 6H2c-.553 0-1 .448-1 1s.447 1 1 1h17.586l-6 6L15 20.414l7.707-7.707c.391-.391.391-1.023 0-1.414z"
											></path>
										</svg>
									</a>
								</header>
							</div>

							<div class="container ">
								<div class="scroller">
									<div class="scroller_inner">
										<div
											class="product-list product-list--vertical product-list--scrollable flickity-enabled"
											tabindex="0"
										>
											<div
												class="flickity-viewport"
												style={{
													height: "412px",
													touchAction: " pan-y",
												}}
											>
												<div
													class="flickity-slider"
													style={{
														left: "0px",

														//   transform: translateX("0%")
													}}
												>
													<div
														class="product-item product-item--vertical 1/5--desk is-selected"
														style={{ position: "absolute", left: "0%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹800</span>
															</span>
														</div>
														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro1.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony BRAVIA 4K HDR Ultra HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk is-selected"
														style={{ position: "absolute", left: "20%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹25</span>
															</span>
														</div>

														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro2.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk is-selected"
														style={{ position: "absolute", left: "40%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹800</span>
															</span>
														</div>
														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro1.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony BRAVIA 4K HDR Ultra HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk is-selected"
														style={{ position: "absolute", left: "60%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹25</span>
															</span>
														</div>

														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro2.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk is-selected"
														style={{ position: "absolute", left: "80%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹800</span>
															</span>
														</div>
														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro1.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony BRAVIA 4K HDR Ultra HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk"
														aria-hidden="true"
														style={{ position: "absolute", left: "100%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹25</span>
															</span>
														</div>

														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro2.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk"
														aria-hidden="true"
														style={{ position: "absolute", left: "120%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹800</span>
															</span>
														</div>
														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro1.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony BRAVIA 4K HDR Ultra HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
													<div
														class="product-item product-item--vertical 1/5--desk"
														aria-hidden="true"
														style={{ position: "absolute", left: "140%" }}
													>
														<div class="product-item_label-list">
															<span class="product-label product-label--on-sale">
																Save
																<span>₹25</span>
															</span>
														</div>

														<a
															href="http://saiproj.000webhostapp.com/ware/product.html"
															class="product-item_image-wrapper "
														>
															<div class="aspect-ratio aspect-ratio--square">
																<img
																	class="product-item_primary-image image--blur-up ls-is-cached lazyloaded"
																	src="./pro_files/pro2.jpg"
																/>
															</div>
														</a>

														<div class="product-item_info">
															<div class="product-item_info-inner">
																<a
																	class="product-item_vendor link"
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																>
																	Sony
																</a>
																<a
																	href="http://saiproj.000webhostapp.com/ware/product.html"
																	class="product-item_title text--strong link"
																>
																	Sony HD TV
																</a>
																<div class="product-item_price-list price-list">
																	<span class="price price--highlight">
																		₹1,398
																	</span>
																	<span class="price price--compare">
																		₹2,198
																	</span>
																</div>
																<span class="product-item_inventory inventory inventory--high">
																	In stock, 80 units
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<button
												class="flickity-button flickity-prev-next-button previous"
												type="button"
												disabled=""
												aria-label="Previous"
											>
												<svg class="flickity-button-icon" viewBox="0 0 100 100">
													<path
														d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z"
														class="arrow"
													></path>
												</svg>
											</button>
											<button
												class="flickity-button flickity-prev-next-button next"
												type="button"
												aria-label="Next"
											>
												<svg class="flickity-button-icon" viewBox="0 0 100 100">
													<path
														d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z"
														class="arrow"
														transform="translate(100, 100) rotate(180) "
													></path>
												</svg>
											</button>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		auth: state.auth,
		cartlist: state.cartlist,
	};
};

export default connect(mapStateToProps)(ProductDetails1);
