import React, { Component } from "react";
import Header from "../common/header";
import { connect } from "react-redux";
import Allcats from "../common/allcats";
import InfiniteScroll from "react-infinite-scroll-component";
import user from "../../services/userservice";
import Carousel from "nuka-carousel";

import OwlCarousel from "react-owl-carousel-safe";

class Allproducts extends Component {
  state = {
    getproducts: [],
    image: {},
    allcats: {},
    prd: "",
    fp: [],
    skip: 0,
    cat: "",
    loader: true,
  };

  async componentDidMount() {
    if (!this.props.getproducts) {
      this.props.history.push("/home");
    }
  }

  fetchData = async () => {
    let gproducts = [];

    if (this.props.history.location.type === "category") {
      if (this.props.location.p && this.props.getproducts) {
        const getpr = this.props.getproducts.filter(
          (i) =>
            (i.subcategory === this.props.location.p.subsubcatname &&
              i.categoryid === this.props.location.item._id) ||
            i.categoryid === this.props.history.location.item._id
        );
        await this.setState({ getproducts: getpr });
      } else {
        return (gproducts = this.props.allproducts);
      }
    } else {
      if (this.state.cat !== this.props.history.location.p.subsubcatname) {
        await this.setState({ cat: "", getproducts: [] });
      }
      if (this.props.location.p) {
        const obj = {
          subcategory: this.props.history.location.p.subsubcatname,
          categoryid: this.props.history.location.ie._id,
          skip: this.state.skip,
        };
        const { data } = await user.lazy(obj);

        if (data.length === 0) {
          // console.log(data);
          this.setState({ loader: "hidden" });
        } else {
          await this.setState({ skip: this.state.skip + 12 });
          let newr = [...this.state.getproducts, ...data];
          await this.setState({
            getproducts: newr,
            cat: this.props.history.location.p.subsubcatname,
          });
        }
      } else {
        gproducts = this.props.allproducts;
      }
    }
  };
  productpage = (item) => {
    this.props.history.push({
      pathname: "/productdetails",
      state: { data: item },
    });
  };
  render() {
    let getproducts = [];
    if (this.props.history.location.p) {
      if (this.state.cat !== this.props.history.location.p.subsubcatname) {
        let propproducts =
          this.props.history.location.type === "category"
            ? this.props.location.p && this.props.getproducts
              ? this.props.getproducts.filter(
                  (i) =>
                    (i.subcategory === this.props.location.p.subsubcatname &&
                      i.categoryid === this.props.location.ie._id) ||
                    i.categoryid === this.props.history.location.ie._id
                )
              : this.props.allproducts
            : this.props.location.p && this.props.getitems
            ? this.props.getitems[this.props.history.location.item].filter(
                (i) =>
                  i.subcategory === this.props.location.p.subsubcatname ||
                  i.categoryid === this.props.history.location.item._id
              )
            : null;

        getproducts = propproducts;
      } else {
        let propproducts =
          this.props.history.location.type === "category"
            ? this.props.location.p && this.props.getproducts
              ? this.props.getproducts.filter(
                  (i) =>
                    i.subcategory === this.props.location.p.subsubcatname &&
                    i.categoryid === this.props.location.item._id
                )
              : this.props.allproducts
            : this.props.location.p && this.props.getitems
            ? this.props.getitems[this.props.history.location.item].filter(
                (i) =>
                  i.subcategory === this.props.location.p.subsubcatname ||
                  i.categoryid === this.props.history.location.item._id
              )
            : null;

        getproducts =
          this.state.getproducts.length > 0
            ? this.state.getproducts
            : propproducts;
      }
    }
    const gp =
      this.props.allproducts && this.props.history.location.p
        ? this.props.allproducts.filter(
            (i) => i.categoryid === this.props.location.ie._id
          )
        : null;

    return (
      <React.Fragment>
        <Header />
        <div id="shopify-section-product-template" className="shopify-section">
          <div className="container container--flush">
            <div className="page_sub-header"></div>
            <div className="layout">
              <div className="layout_section layout_section--secondary hidden-pocket">
                <Allcats history={this.props.history} />
              </div>
              <div className="layout__section">
                {/* <allproducts2 /> */}
                {/* <allproducts/> */}
                <div
                  className="collection__image-wrapper collection__image-wrapper--medium"
                  style={{
                    backgroundImage:
                      "url(https://www.artisstore.com/media/responsivebannerslider/artiswebsitebanner3_1570619470.jpg)",
                    height: "155px",
                  }}
                ></div>
                <div className="card ">
                  <header className="card__header card__header--tight">
                    <div className="collection__header ">
                      <div className="collection__header-inner">
                        <div className="collection__meta">
                          <div className="collection__meta-inner">
                            <h1 className="collection__title heading h1">
                              Showing {getproducts ? getproducts.length : 0}{" "}
                              products
                            </h1>
                          </div>
                        </div>
                      </div>
                    </div>
                  </header>
                </div>
                <InfiniteScroll
                  dataLength={getproducts && getproducts.length}
                  next={this.fetchData.bind(this)}
                  hasMore={true}
                  endMessage={
                    <p style={{ textAlign: "center" }}>
                      <b>Yay! You have seen it all</b>
                    </p>
                  }
                  loader={
                    <div
                      style={{
                        textAlign: "center",
                        width: "100%",
                        visibility: this.state.loader,
                      }}
                    >
                      <img
                        src="https://media.tenor.com/images/7d9cb36e95124fb829ff8f2450c3a567/tenor.gif"
                        alt="loader"
                      />
                    </div>
                  }
                >
                  <div className="product-list product-list--collection product-list--with-sidebar">
                    {getproducts
                      ? getproducts.length > 0 &&
                        getproducts.map((p, index) => {
                          return (
                            <div
                              style={{ cursor: "pointer" }}
                              key={index}
                              onClick={() => this.productpage(p)}
                              className="product-item product-item--vertical 1/4--desk"
                            >
                              <div className="product-item__image-wrapper ">
                                <div className="aspect-ratio aspect-ratio--square">
                                  <img
                                    className="product-item__primary-image lazyload "
                                    src={p.image[0]}
                                    alt="img"
                                  />
                                </div>
                              </div>
                              <div className="product-item__info">
                                <div className="product-item__info-inner">
                                  <p className="product-item__title text--strong link">
                                    {" "}
                                    {p.title.slice(0, 40)}....
                                  </p>
                                  <div className="product-item__price-list price-list mb-2">
                                    <span
                                      // style={{
                                      // 	fontSize: "16px",
                                      // 	color: "blue",
                                      // }}
                                      className="price price--compare"
                                    >
                                      ₹{p.mrp}
                                    </span>
                                    {/* <br /> */}
                                    &nbsp; &nbsp;
                                    <span
                                      className="offset-2"
                                      // style={{
                                      // 	fontSize: "22px",
                                      // 	color: "blue",
                                      // }}
                                    >
                                      ₹{p.dealprice}
                                    </span>
                                  </div>
                                </div>
                                <br />
                                <button
                                  type="button"
                                  className="product-item_action-button  button--small button--primary"
                                >
                                  Buy Now{" "}
                                </button>
                                <button
                                  type="button"
                                  className="product-item_action-button button button--small button--ternary hidden-phone"
                                >
                                  Add to Cart{" "}
                                </button>
                              </div>
                            </div>
                          );
                        })
                      : this.state.loader}
                  </div>
                </InfiniteScroll>
              </div>
            </div>
          </div>
          <div className="shopify-section">
            <section
              className="section"
              data-section-type="featured-collection"
              data-section-settings="{ }"
            >
              <div className="container">
                <header className="section_header">
                  <div className="section_header-stack">
                    <h2 className="section_title heading h3"> Recommended </h2>
                  </div>
                  {/* <a
										href="collections/sales.html"
										className="section_action-link link"
									>
										View all sales
										<svg
											className="icon icon--tail-right"
											viewBox="0 0 24 24"
											role="presentation"
										>
											<path
												fill="currentColor"
												d="M22.707 11.293L15 3.586 13.586 5l6 6H2c-.553 0-1 .448-1 1s.447 1 1 1h17.586l-6 6L15 20.414l7.707-7.707c.391-.391.391-1.023 0-1.414z"
											></path>
										</svg>
									</a> */}
                </header>
              </div>

              <Carousel
                autoplay={true}
                autoplayInterval={5000}
                slidesToScroll={"auto"}
                // slidesToShow={12}
                enableKeyboardControls={true}
                autoGenerateStyleTag={true}
                wrapAround={true}
                renderBottomCenterControls={false}
                renderCenterLeftControls={({ previousSlide }) => (
                  <button className="chbutleft" onClick={previousSlide}>
                    <i className="fas fa-chevron-left"></i>
                  </button>
                )}
                renderCenterRightControls={({ nextSlide }) => (
                  <button className="chbutright" onClick={nextSlide}>
                    <i className="fas fa-chevron-right" />
                  </button>
                )}
              >
                <div
                  className="container "
                  style={{ display: "flex", width: "auto" }}
                >
                  {gp &&
                    gp.slice(0, 40).map((p, index) => {
                      return (
                        <div className="scroller">
                          <div
                            onClick={() => this.productpage(p)}
                            className="product-item product-item--vertical 1/5--desk"
                            style={{
                              width: "240px",
                              // cursor: "pointer"
                            }}
                          >
                            <div className="product-item_label-list">
                              <span className="product-label product-label--on-sale">
                                Save
                                <span>{p.yousave}</span>
                              </span>
                            </div>
                            <div className="product-item_image-wrapper ">
                              <div className="aspect-ratio aspect-ratio--square">
                                <img
                                  className="product-item_primary-image lazyload "
                                  src={p.image}
                                  alt="img"
                                />
                              </div>
                            </div>
                            <div className="product-item_info">
                              <div className="product-item_info-inner">
                                <p className="product-item_title text--strong link">
                                  {p.title.slice(0, 20)}
                                </p>
                                <div className="product-item_price-list price-list">
                                  <span className="price  price--compare">
                                    ₹{p.mrp}
                                  </span>{" "}
                                  &nbsp;
                                  <span className="price price--highlight">
                                    ₹{p.dealprice}
                                  </span>
                                </div>
                                {/* <span className="product-item_inventory inventory inventory--high">
																		In stock {p.qty} units
																	</span> */}
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </Carousel>
            </section>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    allproducts: state.getproducts,
    getproducts: state.getproducts,
    getcategorys: state.getcategorys,
    getitems: state.getitems,
  };
};

export default connect(mapStateToProps)(Allproducts);
