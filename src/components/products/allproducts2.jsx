import React, { Component } from "react";
import Header from "../common/header";
// import Collapsible from "react-collapsible";
// import auth from "../../services/authService";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import get_products from "../../redux/actions/getproductsActions";
import Allcats from "../common/allcats";
// import get_categorys from "../../redux/actions/getCategorysActions";

class Allproducts2 extends Component {
  state = {
    getproducts: [],
    image: {},
    allcats: {},
  };
  componentDidMount = async () => {
    setTimeout(async () => {
      const data = await this.props.getcategorys;
      await this.setState({ allcats: data });
    }, 2000);
    await get_products();
    setTimeout(async () => {
      await this.setState({
        getproducts: this.props.allproducts,
      });
    }, 5000);
  };
  productpage = (item) => {
    this.props.history.push({
      pathname: "/allproducts",
      type: "category",
      p: item,
      ie: this.props.location.item,
      
    });
  };

  render() {
    const prd = this.props.location.it;
    return (
      <React.Fragment>
        <Header />
        <div id="shopify-section-product-template" className="shopify-section">
          <div className="container ">
            <div className="page_sub-header"></div>

            <div className="container--flush">
            <header class="section_header clearfix">
              <div class="section_header-stack">
              <h2 class="section_title heading h3"> {prd ? prd.subcatname : <Redirect to="/home" />} </h2></div>
              </header>
            <div className="row"> 
            {!!prd ? (
                    prd.subsubcat.map((p, index) => {
                      return (
                        <div className="1/4--desk all-category">
                          <div class="grid mr-2" onClick={() => this.productpage()} >              
                              <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT8ed4D57d9VV6TXZixxli1myDZZw-vLdbB4A&usqp=CAU" /> 
                          </div>                 
                          <h3>{p.subsubcatname}</h3> 
                        </div>
                       );
                      })
                    ) : null}

              {/* <div className="1/4--desk all-category">
               <div class="grid mr-2">
                 <img src="https://www.bazaarchalo.com/wp-content/uploads/2019/11/best-running-shoes.jpg"  
                    /></div>
                 
                 <h3>   Accessories  </h3>
              </div>

              <div className="1/4--desk all-category">
               <div class="grid mr-2">
                 <img src="https://www.bazaarchalo.com/wp-content/uploads/2019/11/best-running-shoes.jpg"  
                    /></div>
                 
                 <h3>   Accessories  </h3>
              </div>

              <div className="1/4--desk all-category">
               <div class="grid mr-2">
                 <img src="https://www.bazaarchalo.com/wp-content/uploads/2019/11/best-running-shoes.jpg"  
                    /></div>
                 
                 <h3>   Accessories  </h3>
              </div>
              <div className="1/4--desk all-category">
               <div class="grid mr-2">
                 <img src="https://www.bazaarchalo.com/wp-content/uploads/2019/11/best-running-shoes.jpg"  
                    /></div>
                 
                 <h3>   Accessories  </h3>
              </div> */}

              </div>

          </div>
        </div>
        </div>

        <div className="shopify-section">
          <section
            className="section"
            data-section-type="featured-collection"
            data-section-settings="{ }"
          >
            <div className="container">
              <header className="section_header">
                <div className="section_header-stack">
                  <h2 className="section_title heading h3">
                    {" "}
                    Recently viewed{" "}
                  </h2>
                </div>
                <a
                  href="collections/sales.html"
                  className="section_action-link link"
                >
                  View all sales
                  <svg
                    className="icon icon--tail-right"
                    viewBox="0 0 24 24"
                    role="presentation"
                  >
                    <path
                      fill="currentColor"
                      d="M22.707 11.293L15 3.586 13.586 5l6 6H2c-.553 0-1 .448-1 1s.447 1 1 1h17.586l-6 6L15 20.414l7.707-7.707c.391-.391.391-1.023 0-1.414z"
                    ></path>
                  </svg>
                </a>
              </header>
            </div>

            <div className="container ">
              <div className="scroller">
                <div className="scroller_inner">
                  <div className="product-list product-list--vertical product-list--scrollable ">
                    <div className="product-item product-item--vertical 1/5--desk">
                      <div className="product-item_label-list">
                        <span className="product-label product-label--on-sale">
                          Save
                          <span>₹800</span>
                        </span>
                      </div>
                      <div className="product-item_image-wrapper ">
                        <div className="aspect-ratio aspect-ratio--square">
                          <img
                            className="product-item_primary-image lazyload image--blur-up"
                            src="images/products/pro1.jpg"
                            alt="img"
                          />
                        </div>
                      </div>

                      <div className="product-item_info">
                        <div className="product-item_info-inner">
                          <p className="product-item_vendor link">Sony</p>
                          <p className="product-item_title text--strong link">
                            Sony BRAVIA 4K HDR Ultra HD TV
                          </p>
                          <div className="product-item_price-list price-list">
                            <span className="price price--highlight">
                              ₹1,398
                            </span>
                            <span className="price price--compare">₹2,198</span>
                          </div>
                          <span className="product-item_inventory inventory inventory--high">
                            In stock, 80 units
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </React.Fragment>
    );
  }
}

// export default Allproducts;
const mapStateToProps = (state) => {
  return {
    allproducts: state.getproducts,
    getcategorys: state.getcategorys,
  };
};
export default connect(mapStateToProps)(Allproducts2);
