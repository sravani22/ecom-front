import React, { Component } from "react";
import { connect } from "react-redux";
import Allproducts from "./allproducts";
import Allproducts2 from "./allproducts2";
// import get_categorys from "../../redux/actions/getCategorysActions";
import Header from "../common/header";
// import Collapsible from "react-collapsible";
// import auth from "../../services/authService";

import { Switch, Route, Redirect, Link } from "react-router-dom";
import Allcats from "../common/allcats";

class Products extends Component {
  state = {
    allcats: [],
  };
  componentDidMount = async () => {
    // if (!this.props.getcategorys.length > 0) {
    // 	// await get_categorys();
    // }
    // setTimeout(async () => {
    // 	const data = await this.props.getcategorys;
    // 	await this.setState({ allcats: data });
    // }, 2000);
  };
  render() {
    // const { allcats, getproducts } = this.state;
    return (
      <React.Fragment>
        <Header />
        <div id="shopify-section-product-template" className="shopify-section">
          <div className="container container--flush">
            <div className="page_sub-header"></div>
            <div className="layout">
              <div className="layout_section layout_section--secondary hidden-pocket">
                <Allcats />
              </div>
              <Switch>
                <Route exact path="/" component={Allproducts} />
                <Route exact path="/allproducts" component={Allproducts} />
                <Route exact path="/allproducts2" component={Allproducts2} />
              </Switch>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getcategorys: state.getcategorys,
  };
};
export default connect(mapStateToProps)(Products);
