import React, { Component } from "react";
import { toast } from "react-toastify";
import users from "../../services/userservice";
import Header from "../common/header";

class Activation extends Component {
	state = {
		parseddata: "",
		token: "",
	};
	componentDidMount = async () => {
		toast.configure();
		toast.success("Activated Successfully", {
			position: toast.POSITION.BOTTOM_LEFT,
		});
		if (this.props.match.params.id !== "") {
			await this.setState({ token: this.props.match.params.id });
			window.history.replaceState(null, null, window.location.pathname);
			toast.configure();
			try {
				const obj = {
					token: this.state.token,
				};
				const data = await users.activate(obj);
				if (data.data.success) {
					toast.success(data.data.success, {
						position: toast.POSITION.BOTTOM_LEFT,
					});
					this.props.history.push("/home");
				}
			} catch (ex) {
				if (ex.response && ex.response.status === 400) {
					toast.error(ex.response.data, {
						position: toast.POSITION.BOTTOM_LEFT,
					});
				}
			}
		}
	};
	render() {
		return (
			<React.Fragment>
				<Header history={this.props.history} />
				<div class="site-account" id="CustomerLoginForm">
					<div class="title-wrapper text-center mt-4">
						<ol className="breadcrumbs"></ol>
						<h1
							className="page-title"
							style={{
								opacity: 1,
								transform: "matrix(1, 0, 0, 1, 0, 0)",
							}}
						>
							Activate
						</h1>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

export default Activation;
