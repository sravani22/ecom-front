import React, { Component } from "react";
import Header from "../common/header";
import { Link } from "react-router-dom";
import cart from "../../services/cartService";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import get_address from "../../redux/actions/getaddressActions";

var Joi = require("joi-browser");

const schema = Joi.object().keys({
  fullname: Joi.string().required(),
  doornumber: Joi.string().required(),
  street: Joi.string().required(),
  area: Joi.string().required(),
  landmark: Joi.string().required(),
  city: Joi.string().required(),
  state: Joi.string().required(),
  pincode: Joi.string().required(),
  mobile: Joi.string().min(4).max(10).required(),
});
class Checkout extends Component {
  state = {
    fullname: "",
    doornumber: "",
    street: "",
    area: "",
    landmark: "",
    city: "",
    state: "",
    pincode: "",
    mobile: "",
    updata: {},
    update: false,
    Indstates: [
      { id: 0, value: "State" },
      { id: 1, value: "Jammu and Kashmir" },
      { id: 2, value: "Punjab" },
      { id: 3, value: "Himachal Pradesh" },
      { id: 4, value: "Haryana" },
      { id: 5, value: "Delhi" },
      { id: 6, value: "Rajasthan" },
      { id: 7, value: " Uttar Pradesh" },
      { id: 8, value: "Uttarakhand" },
      { id: 9, value: " Madhya Pradesh" },
      { id: 10, value: "Chattisgarh" },
      { id: 11, value: "Gujarat" },
      { id: 12, value: "Maharashtra" },
      { id: 13, value: "Karnataka" },
      { id: 14, value: "Goa" },
      { id: 15, value: "Kerala" },
      { id: 16, value: "Tamil nadu" },
      { id: 17, value: "Andhra pradesh" },
      { id: 18, value: "Telangana" },
      { id: 19, value: "Orissa" },
      { id: 20, value: "Bihar" },
      { id: 21, value: "Jharkhand" },
      { id: 22, value: "West Bengal" },
      { id: 23, value: "Assam" },
      { id: 24, value: "Arunach Pradesh" },
      { id: 25, value: "Sikkim" },
      { id: 26, value: "Meghalaya" },
      { id: 27, value: "Mizoram" },
      { id: 28, value: "Nagaland" },
      { id: 29, value: "Tripura" },
    ],
  };
  componentDidMount = async () => {
    get_address();
  };
  handleChange = async (evt) => {
    await this.setState({ [evt.target.name]: evt.target.value });
  };
  addaddress = async () => {
    toast.configure();
    const {
      fullname,
      doornumber,
      street,
      area,
      landmark,
      city,
      state,
      pincode,
      mobile,
    } = this.state;

    const validata = Joi.validate(
      {
        fullname,
        doornumber,
        street,
        area,
        landmark,
        city,
        state,
        pincode,
        mobile,
      },
      schema,
      function (err, value) {
        if (!err) return null;
        const reterr = err.details[0].message;
        return reterr;
      }
    );
    if (!!validata) {
      toast.error(validata, {
        position: toast.POSITION.BOTTOM_LEFT,
      });
      setTimeout(async () => {
        await this.setState({
          fullname: "",
          doornumber: "",
          street: "",
          area: "",
          landmark: "",
          city: "",
          state: "",
          pincode: "",
          mobile: "",
        });
      }, 2000);
    } else {
      try {
        const obj = {
          fullname: this.state.fullname,
          drnum: this.state.doornumber,
          street: this.state.street,
          area: this.state.area,
          city: this.state.city,
          state: this.state.state,
          pincode: this.state.pincode,
          other: this.state.landmark,
          contact: this.state.mobile,
        };

        const data = await cart.addaddress(obj);
        if (data.data.success) {
          toast.success(data.data.success, {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          get_address();
          setTimeout(async () => {
            await this.setState({
              fullname: "",
              doornumber: "",
              street: "",
              area: "",
              landmark: "",
              city: "",
              state: "",
              pincode: "",
              mobile: "",
            });
          }, 2000);
        }
      } catch (ex) {
        if (ex.response && ex.response.status === 400) {
          toast.error(ex.response.data, {
            position: toast.POSITION.BOTTOM_LEFT,
          });
          setTimeout(async () => {
            await this.setState({
              fullname: "",
              doornumber: "",
              street: "",
              area: "",
              landmark: "",
              city: "",
              state: "",
              pincode: "",
              mobile: "",
            });
          }, 2000);
        }
      }
    }
  };
  changeaddress = async (item) => {
    await this.setState({ update: true, updata: item });
  };
  updateaddress = async () => {
    toast.configure();
    const {
      fullname,
      doornumber,
      street,
      area,
      landmark,
      city,
      state,
      pincode,
      mobile,
      updata,
    } = this.state;
    const upobj = {
      adrsid: updata.adrsid,
      fullname: fullname ? fullname : updata.fullname,
      drnum: doornumber ? doornumber : updata.drnum,
      street: street ? street : updata.street,
      area: area ? area : updata.area,
      city: city ? city : updata.city,
      state: state ? state : updata.state,
      pincode: pincode ? pincode : updata.pincode,
      other: landmark ? landmark : updata.other,
      contact: mobile ? mobile : updata.mobile,
    };

    try {
      const data = await cart.updateaddress(upobj);
      if (data) {
        await this.setState({ update: false });
        toast.success("Updated Successfully", {
          position: toast.POSITION.BOTTOM_LEFT,
        });
        get_address();
        await this.setState({
          fullname: "",
          doornumber: "",
          street: "",
          area: "",
          landmark: "",
          city: "",
          state: "",
          pincode: "",
          mobile: "",
        });
      }
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        toast.error(ex.response.data, {
          position: toast.POSITION.BOTTOM_LEFT,
        });
        setTimeout(async () => {
          await this.setState({
            fullname: "",
            doornumber: "",
            street: "",
            area: "",
            landmark: "",
            city: "",
            state: "",
            pincode: "",
            mobile: "",
          });
        }, 2000);
      }
    }
  };

  deleteaddress = async (item, evt) => {
    toast.configure();
    if (item) {
      const obj = {
        adrsid: item.adrsid,
      };
      const data = await cart.deleteaddress(obj);
      if (data.data.success) {
        toast.success(data.data.success, {
          position: toast.POSITION.BOTTOM_LEFT,
        });
      }
      await get_address();
    }
  };
  render() {
    const states = this.state.Indstates;
    const getaddress = this.props.getaddress;

    return (
      <React.Fragment>
        <Header history={this.props.history} />
        <div className="container">
          <div className="page_sub-header">
            <header className="page_header page__header--stack">
              <h1 className="page_title heading h1"> Checkout </h1>
            </header>
          </div>
        </div>
        <div className="cart-wrapper">
          <div className="cart-wrapper_inner">
            <div className="cart-wrapper_inner-inner">
              <div className="container container--flush">
                <div className="card">
                  <div className="card__header">
                    <h2 className="card__title heading h3">
                      Shipping Address{" "}
                    </h2>
                  </div>
                  <div>
                    {getaddress && getaddress.length > 0 ? (
                      getaddress.map((ad, index) => {
                        return (
                          <div
                            key={index}
                            className="product-list product-list--collection card__section "
                          >
                            {/* <div className="mr-4">
															<input
																onSelect={this.handleChange}														
																type="radio"
																className="radio"
																id="filter-3-tag-brand_akg"
																name="tag-brand"
																value="checked"
																checked={this.props.getaddress.adrsid} 
																// onChange={this.handleChange}	
															/>
														</div> */}
                            <div className="1/2--desk">
                              <div>
                                <h2
                                  style={{
                                    fontWeight: "bold",
                                    textTransform: "capitalize",
                                  }}
                                >
                                  {ad.fullname}
                                </h2>
                                <p className="">
                                  {" "}
                                  {ad.drnum}, {ad.street}, {ad.area},{ad.city},{" "}
                                  {ad.state},{ad.pincode}
                                  <br /> {ad.contact}
                                </p>
                              </div>
                            </div>

                            <div className="1/3--desk card__section text-right ml-5">
                              <button
                                onClick={() => this.changeaddress(ad)}
                                type="submit"
                                className="button button--primary button--small mr-2"
                              >
                                Edit
                              </button>
                              <button
                                onClick={() => {
                                  if (window.confirm("Delete the address?"))
                                    this.deleteaddress(ad);
                                }}
                                type="submit"
                                className="button button-danger button--small"
                              >
                                Delete
                              </button>
                            </div>
                          </div>
                        );
                      })
                    ) : (
                      <h4 className="text-center mt-4">No Address Found</h4>
                    )}
                    {!this.state.update ? (
                      <div className="card__section ">
                        <div className="form__header">
                          <h2 className="heading h3">
                            Add new Shipping address{" "}
                          </h2>{" "}
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="text"
                              className="form__field form__field--text "
                              name="fullname"
                              value={this.state.fullname}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Full Name
                            </label>
                          </div>
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="text"
                              className="form__field form__field--text "
                              name="doornumber"
                              value={this.state.doornumber}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              DOOR.No
                            </label>
                          </div>

                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-email"
                              type="email"
                              className="form__field form__field--text "
                              name="street"
                              value={this.state.street}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-email"
                              className="form__floating-label"
                            >
                              Street
                            </label>
                          </div>
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-email"
                              type="email"
                              className="form__field form__field--text "
                              name="area"
                              value={this.state.area}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-email"
                              className="form__floating-label"
                            >
                              Area
                            </label>
                          </div>
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="text"
                              className="form__field form__field--text "
                              name="landmark"
                              value={this.state.landmark}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Land Mark
                            </label>
                          </div>

                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-email"
                              className="form__field form__field--text "
                              name="city"
                              value={this.state.city}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-email"
                              className="form__floating-label"
                            >
                              City
                            </label>
                          </div>
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <div className="select-wrapper select-wrapper--primary">
                              <svg
                                className="icon icon--arrow-bottom"
                                viewBox="0 0 12 8"
                                role="presentation"
                              >
                                <path
                                  stroke="currentColor"
                                  stroke-width="2"
                                  d="M10 2L6 6 2 2"
                                  fill="none"
                                  stroke-linecap="square"
                                ></path>
                              </svg>
                              <select
                                value={this.state.state}
                                onChange={this.handleChange}
                                name="state"
                              >
                                {states
                                  ? states.map((st, id) => {
                                      return (
                                        <optgroup key={id}>
                                          <option value={st.value}>
                                            {" "}
                                            {st.value}{" "}
                                          </option>
                                        </optgroup>
                                      );
                                    })
                                  : null}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="number"
                              className="form__field form__field--text "
                              name="pincode"
                              value={this.state.pincode}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Pin Code
                            </label>
                          </div>
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="number"
                              className="form__field form__field--text "
                              name="mobile"
                              value={this.state.mobile}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Mobile No
                            </label>
                          </div>
                        </div>

                        {/* <div className="form__input-wrapper form__input-wrapper--labelled">
													<div className="checkbox-wrapper">
														<input
															type="checkbox"
															className="checkbox"
															name="tag-price"
														/>
														<svg
															className="icon icon--check"
															viewBox="0 0 24 24"
															role="presentation"
														>
															<path
																fill="currentColor"
																d="M9 20l-7-7 3-3 4 4L19 4l3 3z"
															></path>
														</svg>
													</div>
													<label>Save this information for next time </label>
												</div> */}
                        <div
                          onClick={this.addaddress}
                          className="form__submit  button button--primary button--min-width"
                        >
                          {" "}
                          Save & Continue to shipping
                        </div>
                      </div>
                    ) : (
                      <div className="card__section ">
                        <div className="form__header">
                          {" "}
                          <h2 className="heading h3">
                            {" "}
                            Update Shipping address{" "}
                          </h2>{" "}
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="text"
                              className="form__field form__field--text "
                              name="fullname"
                              placeholder={this.state.updata.fullname}
                              value={this.state.fullname}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Full Name
                            </label>
                          </div>
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="text"
                              className="form__field form__field--text "
                              name="doornumber"
                              placeholder={this.state.updata.drnum}
                              value={this.state.doornumber}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              DOOR.No
                            </label>
                          </div>

                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-email"
                              type="email"
                              className="form__field form__field--text "
                              name="street"
                              placeholder={this.state.updata.street}
                              value={this.state.street}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-email"
                              className="form__floating-label"
                            >
                              Street
                            </label>
                          </div>
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-email"
                              type="email"
                              className="form__field form__field--text "
                              name="area"
                              placeholder={this.state.updata.area}
                              value={this.state.area}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-email"
                              className="form__floating-label"
                            >
                              Area
                            </label>
                          </div>
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="text"
                              className="form__field form__field--text "
                              name="landmark"
                              placeholder={this.state.updata.other}
                              value={this.state.landmark}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Land Mark
                            </label>
                          </div>

                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-email"
                              className="form__field form__field--text "
                              name="city"
                              placeholder={this.state.updata.city}
                              value={this.state.city}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-email"
                              className="form__floating-label"
                            >
                              City
                            </label>
                          </div>
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <div className="select-wrapper select-wrapper--primary">
                              <svg
                                className="icon icon--arrow-bottom"
                                viewBox="0 0 12 8"
                                role="presentation"
                              >
                                <path
                                  stroke="currentColor"
                                  stroke-width="2"
                                  d="M10 2L6 6 2 2"
                                  fill="none"
                                  stroke-linecap="square"
                                ></path>
                              </svg>
                              <select
                                value={this.state.state}
                                onChange={this.handleChange}
                                placeholder={this.state.updata.state}
                                name="state"
                              >
                                {states
                                  ? states.map((st, id) => {
                                      return (
                                        <optgroup key={id}>
                                          <option
                                            value={
                                              st.value === "State"
                                                ? this.state.updata.state
                                                : st.value
                                            }
                                          >
                                            {" "}
                                            {st.value === "State"
                                              ? this.state.updata.state
                                              : st.value}{" "}
                                          </option>
                                        </optgroup>
                                      );
                                    })
                                  : null}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="form__input-row">
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="number"
                              className="form__field form__field--text "
                              placeholder={this.state.updata.pincode}
                              name="pincode"
                              value={this.state.pincode}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Pin Code
                            </label>
                          </div>
                          <div className="form__input-wrapper form__input-wrapper--labelled">
                            <input
                              id="contact-form-name"
                              type="number"
                              className="form__field form__field--text "
                              name="mobile"
                              placeholder={this.state.updata.contact}
                              value={this.state.mobile}
                              onChange={this.handleChange}
                            />
                            <label
                              for="contact-form-name"
                              className="form__floating-label"
                            >
                              Mobile No
                            </label>
                          </div>
                        </div>

                        {/* <div className="form__input-wrapper form__input-wrapper--labelled">
													<div className="checkbox-wrapper">
														<input
															type="checkbox"
															className="checkbox"
															name="tag-price"
														/>
														<svg
															className="icon icon--check"
															viewBox="0 0 24 24"
															role="presentation"
														>
															<path
																fill="currentColor"
																d="M9 20l-7-7 3-3 4 4L19 4l3 3z"
															></path>
														</svg>
													</div>
													<label>Save this information for next time </label>
												</div> */}
                        <div
                          onClick={this.updateaddress}
                          className="form__submit  button button--primary button--min-width"
                        >
                          {" "}
                          Update
                        </div>
                      </div>
                    )}
                  </div>
                </div>

                <div className="cart-recap ">
                  <div className="cart-recap_scroller">
                    <div className="card">
                      <div className="card_section">
                        <div className="cart-recap_price-line text--pull">
                          <span className="cart-recap_price-line-">Items </span>
                          <span className="cart-recap_price-line-price">
                            ₹117.00
                          </span>
                        </div>
                        <hr className="card__separator" />
                        <div className="cart-recap_price-line text--pull">
                          <span> Shipping & Handling </span>
                          <span className="cart-recap_price-line-price">
                            ₹10.95
                          </span>
                        </div>
                        <hr className="card__separator" />
                        <div className="cart-recap_price-line text--pull">
                          <span className="cart-recap_price-line-label">
                            {" "}
                            Estimated Tax{" "}
                          </span>
                          <span className="cart-recap_price-line-price">
                            ₹1.95
                          </span>
                        </div>
                        <hr className="card__separator" />
                        <div className="cart-recap_price-line text--pull">
                          <span className="cart-recap_price-line-label">
                            {" "}
                            Total Order{" "}
                          </span>
                          <span className="cart-recap_price-line-price">
                            ₹122.95
                          </span>
                        </div>
                        <div className="cart-recap_notices rte"></div>
                        <button
                          type="submit"
                          className="button button--primary button--full button--large"
                        >
                          {" "}
                          Place your Order
                        </button>
                        <div className="cart-recap_note">
                          <button
                            type="button"
                            className="cart-recap__note-button"
                            data-action="toggle-collapsible"
                            aria-controls="order-note"
                            aria-expanded="false"
                          >
                            Payment Method{" "}
                            <span>
                              <span className="cart-recap__note-edit ">
                                Edit
                              </span>{" "}
                              <i className="fas fa-angle-down"> </i>{" "}
                            </span>
                          </button>
                          <div
                            id="order-note"
                            className="collapsible"
                            aria-hidden="true"
                          >
                            Lorem Ipsum is simply dummy text of the printing and
                            typesetting industry.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getaddress: state.getaddress,
  };
};
export default connect(mapStateToProps)(Checkout);
