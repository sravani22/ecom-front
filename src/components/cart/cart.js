import React, { Component } from "react";
import cart from "../../services/cartService";

class Cart extends Component {
	state = {
		cproduct: [],
	};
	async componentDidMount() {
		const cartdet = await cart.getCart();

		// const cartdet = await this.props.location.product;

		this.setState({ cproduct: cartdet.data });
	}

	render() {
		const { cproduct } = this.state;
		return (
			<React.Fragment>
				<section
					className="offset-1"
					style={{
						marginTop: "200px",
						border: "10x solid black",
						background: "white",
					}}
				>
					<h3>My cart</h3>
					<div class="card" style={{ width: "65%" }}>
						<table class=" card-header">
							<thead class="card-title">
								<tr>
									<th scope="col">Product</th>
									<th scope="col">Quantity</th>
									<th scope="col">Total</th>
								</tr>
							</thead>

							<tbody>
								{cproduct.map(item => (
									<tr>
										<th scope="row">
											<img
												src={item.image}
												style={{ width: "90px", height: "90px" }}
												alt="img"
											/>
											{item.title}
										</th>
										<td>
											<div
												class="btn-group "
												role="group"
												aria-label="Basic example"
											>
												<button type="button" class="btn  btn-group-sm">
													-
												</button>
												<button type="button" class="btn  btn-group-sm">
													{item.quantity}
												</button>
												<button type="button" class="btn  btn-group-sm">
													+
												</button>
											</div>
										</td>
										<td></td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				</section>
			</React.Fragment>
		);
	}
}

export default Cart;
