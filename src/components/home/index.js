import React, { Component } from "react";
import HomeCarousel from "./homecarousel";
import RecentProducts from "../products/recentproducts";
import Topproducts from "../products/topproducts";
import Storeinfo from "../common/storeinfo";
import Header from "../common/header";
import { connect } from "react-redux";
import Dealzone from "./dealzone";
import get_categorys from "../../redux/actions/getCategorysActions";

class Home extends Component {
  state = {};
  componentDidMount = async () => {};
  render() {
    return (
      <React.Fragment>
        <Header history={this.props.history} />
        <HomeCarousel />
        <RecentProducts history={this.props.history} />
        <Dealzone />
        <Topproducts history={this.props.history} />
        {/* <Storeinfo /> */}
        {/* <Contactus /> */}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    getcategorys: state.getcategorys,
    cartlist: state.cartlist,
    getproducts: state.getproducts,
  };
};
export default connect(mapStateToProps)(Home);
