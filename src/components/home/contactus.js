import React, { Component } from "react";
import { Link } from "react-router-dom";

class Contactus extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="row no-gutters ">
          <div className="col-md-12">
            <div className="row no-gutters ">
              <div className="col-md-6">
                <div className="contact-bg">
                  <div
                    class="banner__text"
                    style={{
                      top: "17%",
                      maxHeight: "83%",
                      left: "15%",
                      maxWidth: "85%"
                    }}
                  >
                    <h4
                      class="banner__title weight-600 size-32 font-heading"
                      style={{
                        color: "#000000",
                        lineHeight: "1.2",
                        letterSpacing: "0px",
                        marginBottom: "24px"
                      }}
                    >
                      Visit Us
                    </h4>
                    <div
                      class="banner__caption weight-300 size-22 font-body"
                      style={{
                        color: "#000000",
                        lineHeight: "1.7",
                        letterSpacing: "0px",
                        marginBottom: "35px"
                      }}
                    >
                      Head Office
                      <br />
                      16 Boulevard Saint-Germain
                      <br />
                      75005 Paris
                    </div>
                    <Link
                      href="#"
                      className="btn btn-medium size-13 weight-700 font-heading btn-link"
                      style={{
                        color: "#000000",
                        backgroundColor: "#00c9b7",
                        borderColor: "#000000"
                      }}
                    >
                      Contact us
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="contact-bgright">
                  <div
                    class="banner__text"
                    style={{
                      top: "17%",
                      maxHeight: "83%",
                      left: "10%",
                      maxWidth: "90%"
                    }}
                  >
                    <h4
                      class="banner__title weight-600 size-32 font-heading"
                      style={{
                        color: "#000000",
                        lineHeight: 1.2,
                        letterSpacing: "0px",
                        marginBottom: "35px"
                      }}
                    >
                      Follow Our Store
                      <br />
                      On Instagram
                    </h4>
                    <Link
                      href=""
                      class="btn btn-medium size-13 weight-700 font-heading btn-link"
                      style={{
                        color: "#655655",
                        backgroundColor: "#00c9b7",
                        borderColor: "#655655"
                      }}
                    >
                      @roartheme
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Contactus;
