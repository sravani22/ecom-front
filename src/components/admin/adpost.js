import React from "react";
import Joi from "joi-browser";
import Form from "../common/form";
import { saveProduct } from "../../services/productService";

class Adpost extends Form {
	state = {
		data: {
			title: "",
			description: "",
			image: [],
			mrp: "",
			dealprice: "",
			typeinfo: "",
			stock: "",
		},
		categories: [
			{ id: 1, name: "--Select--" },
			{ id: 2, name: "Computers" },
			{ id: 3, name: "Service" },
			{ id: 4, name: "Clothing" },
			{ id: 5, name: "Restaurant" },
			{ id: 6, name: "Travel" },
			{ id: 7, name: "Electronics" },
			{ id: 8, name: "Mobile" },
			{ id: 9, name: "Health/Fitness" },
			{ id: 10, name: "Games" },
			{ id: 11, name: "Automobiles" },
			{ id: 12, name: "Furniture" },
			{ id: 13, name: "Home & Kitchen" },
			{ id: 14, name: "Appliances" },
			{ id: 15, name: "Footwear" },
			{ id: 16, name: "Books" },
			{ id: 17, name: "Accessories" },
		],
		errors: {},
		showmsg: false,
	};

	schema = {
		title: Joi.string().required(),
		category: Joi.string().required(),
		description: Joi.string()
			.required()
			.max(500),
		image: Joi.string().required(),
		mrp: Joi.string().required(),
		dealprice: Joi.string().required(),
		typeinfo: Joi.string().required(),
		stock: Joi.string().required(),
	};
	doSubmit = async () => {
		try {
			const { data } = this.state;

			const addata = await saveProduct(data);

			if (addata.data.success) {
				this.setState({ showmsg: true });
				setTimeout(() => {
					this.setState({ showmsg: false, data: "" });
				}, 2000);
			}
		} catch (error) {}
	};

	render() {
		return (
			<React.Fragment>
				{this.state.showmsg ? (
					<div className="wrapper wrapper2">
						<h1 className="heading-primary">Ad Posted Successfully</h1>
					</div>
				) : (
					<section className="sptb">
						<div className="container">
							<div className="row ">
								<div className="col-lg-8 col-md-12 col-md-12">
									<div className="card ">
										<form onSubmit={this.handleSubmit}>
											<div className="card-header ">
												<h3 className="card-title">Add Post</h3>
											</div>
											<div className="card-body mt-4">
												{this.renderInput("title", "Title", "title")}
												{this.renderSelect(
													"category",
													"Category",
													this.state.categories,
												)}
												{this.renderInput(
													"description",
													"Description",
													"description",
												)}
												{this.renderInput("image", "ImageURL")}
												{this.renderInput("mrp", "MRP", "mrp")}
												{this.renderInput(
													"dealprice",
													"DealPrice",
													"dealprice",
												)}
												{this.renderInput("typeinfo", "TypeInfo", "typeinfo")}
												{this.renderInput("stock", "Stock", "stock")}
											</div>
											<div className="card-footer ">
												{this.renderButton("Submit")}
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</section>
				)}
			</React.Fragment>
		);
	}
}

export default Adpost;
