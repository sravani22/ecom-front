import React from "react";

const Select = ({ name, label, options, error, ...rest }) => {
  return (
    <div>
      <select
        name={name}
        id={name}
        {...rest}
        className="form-control input-lg br-tr-md-0 br-br-md-0"
      >
        <optgroup>
          {options.map(option => (
            <option key={option.id} value={option.value}>
              {option.name}
            </option>
          ))}
        </optgroup>
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Select;
