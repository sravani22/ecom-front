import React, { Component } from "react";

class Storeinfo extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div id="shopify-section-1560179897905" className="shopify-section">
          <section
            className="section"
            data-section-type="map"
            data-section-settings='{"markerColor": "#000000"}'
          >
            <div className="container">
              <header className="section_header">
                <h2 className="section_title heading h3">Our stores</h2>
              </header>
              <div className="map">
                <div className="map_store-list">
                  <div className="map_store-item">
                    <button
                      className="map_store-name heading h4"
                      aria-expanded="true"
                      data-action="toggle-store"
                    >
                      HiDEF Lifestyle Home Theater Store{" "}
                      <i className="fas fa-angle-up"> </i>
                    </button>
                    <div
                      className="map_store-collapsible"
                      id="block-1560179897905-0"
                      style={{ height: "auto" }}
                    >
                      <div className="map_store-inner">
                        <div className="map_store-address rte">
                          <p>6195 Allentown Blvd, Harrisburg, PA 17112, USA</p>
                        </div>
                        <div className="map_store-hours rte">
                          <p>
                            Monday - Friday: 9AM - 7PM
                            <br />
                            Saturday: 9AM - 5PM
                            <br />
                            Sunday: Closed
                          </p>
                        </div>
                        <a
                          href="/"
                          target="_blank"
                          rel="nofollow noopener"
                          className="map_direction-link button button--primary"
                        >
                          Get directions
                        </a>
                        <div className="map_map-container map_map-container--mobile hidden-tablet-and-up lazyload"></div>
                      </div>
                    </div>
                  </div>
                  <div className="map_store-item" data-store-index="1">
                    <button
                      className="map_store-name heading h4"
                      aria-expanded="false"
                      aria-controls="block-1560180355538"
                      data-action="toggle-store"
                    >
                      HiDEF Showroom
                      <span className="map_icon-container">
                        {" "}
                        <i className="fas fa-angle-up"> </i>{" "}
                      </span>
                    </button>
                    <div
                      className="map_store-collapsible"
                      id="block-1560180355538"
                    >
                      <div className="map_store-inner">
                        <div className="map_store-address rte">
                          <p>Allentown Blvd, Pennsylvania, USA</p>
                        </div>
                        <div className="map_store-hours rte">
                          <p>
                            Monday - Friday: 10AM - 9PM
                            <br />
                            Saturday: 11AM - 9PM
                            <br />
                            Sunday: Closed
                          </p>
                        </div>
                        <a
                          href="/"
                          target="_blank"
                          rel="nofollow noopener"
                          className="map_direction-link button button--primary"
                        >
                          Get directions
                        </a>
                        <div className="map_map-container map_map-container--mobile hidden-tablet-and-up lazyload">
                          <div className="map_gmap"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="map_map-container map_map-container--desktop hidden-phone lazyload">
                  <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d61205.02205290917!2d80.60986484512064!3d16.510244177085514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a35eff9482d944b%3A0x939b7e84ab4a0265!2sVijayawada%2C%20Andhra%20Pradesh!5e0!3m2!1sen!2sin!4v1583480016717!5m2!1sen!2sin"
                    width="100%"
                    height="450"
                    frameBorder="0"
                    style={{ border: 0 }}
                    allowFullScreen=""
                    title="map"
                  ></iframe>
                </div>
              </div>
            </div>
          </section>
        </div>
      </React.Fragment>
    );
  }
}

export default Storeinfo;
