<DropdownMenu className="custdrop">
	<DropdownItem toggle={false}>
		{/* <div className="popover__panel-list">
																<div className="popover__inner">
																	<div className="mini-cart__content mini-cart__content--empty">
																		<p className="alert alert--tight alert--center text--strong">
																			Your Eligible For Free Shipping
																		</p>
																		<div className="mini-cart__empty-state">
																			<svg
																				width="81"
																				height="70"
																				viewBox="0 0 81 70"
																			>
																				<g
																					transform="translate(0 2)"
																					strokeWidth="4"
																					stroke="#1e2d7d"
																					fill="none"
																					fillRule="evenodd"
																				>
																					<circle
																						strokeLinecap="square"
																						cx="34"
																						cy="60"
																						r="6"
																					></circle>
																					<circle
																						strokeLinecap="square"
																						cx="67"
																						cy="60"
																						r="6"
																					></circle>
																					<path d="M22.9360352 15h54.8070373l-4.3391876 30H30.3387146L19.6676025 0H.99560547"></path>
																				</g>
																			</svg>
																			<p className="heading h4">
																				Your cart is empty
																			</p>
																		</div>

																		<a
																			onClick={() => this.allproducts()}
																			className="button button--primary button--full"
																		>
																			Shop our products
																		</a>
																	</div>
																</div>
															</div> */}
		<div className="mini-cart__content">
			<div className="mini-cart__alert-wrapper">
				<p className="alert alert--tight alert--center text--strong">
					You are eligible for free shipping!
				</p>
			</div>
			{!this.state.cartloader ? (
				prod && prd && prd.length > 0 ? (
					prd.slice(0, 3).map((item, index) => {
						return (
							<div
								key={index}
								// onClick={() =>
								//   this.productpage(item)
								// }
								className="mini-cart__line-item-list"
							>
								<div className="mini-cart__line-item">
									{/* <div className="mini-cart__image-wrapper">
																							<div
																								className="aspect-ratio"
																								style={{
																									paddingBottom: "100.0%",
																								}}
																							>
																								<img
																									src={item.image}
																									alt="img"
																								/>
																							</div>
																						</div> */}

									<div className="mini-cart__item-wrapper">
										<div className="mini-cart__product-info">
											{item.title
												? item.title.length >= 50
													? item.title.slice(0, 50)
													: item.title
												: null}

											<div className="mini-cart__price-list">
												<span className="price">{/* ₹{item.deal} */}</span>
											</div>
										</div>
										<div className="mini-cart__quantity">
											<div className="quantity-selector">
												<button
													className="quantity-selector__button"
													onClick={() => this.removeitem(item)}
												>
													<svg
														className="icon icon--minus"
														viewBox="0 0 10 2"
														role="presentation"
													>
														<path d="M10 0v2H0V0z" fill="currentColor"></path>
													</svg>
												</button>
												<input
													className="quantity-selector__value"
													value={item.qty}
													size="2"
												/>
												<button className="quantity-selector__button">
													<svg
														className="icon icon--plus"
														viewBox="0 0 10 10"
														role="presentation"
													>
														<path
															d="M6 4h4v2H6v4H4V6H0V4h4V0h2v4z"
															fill="currentColor"
															fill-rule="evenodd"
														></path>
													</svg>
												</button>
											</div>
											{/* <Link
                                                    onClick={() =>
                                                      this.deletecartid(item)
                                                    }
                                                    data-action="decrease-quantity"
                                                    data-quantity="0"
                                                    data-line-id="17549718388787:ff8fd312f1435be00382509901c3a50d"
                                                    className="mini-cart__quantity-remove link"
                                                  >
                                                    Remove
                                                  </Link> */}
										</div>
									</div>
								</div>
							</div>
						);
					})
				) : (
					<div className="text-center mt-2 mb-2">
						<p style={{ color: "#0001563" }}>No CartItems Available</p>
					</div>
				)
			) : (
				<div className="text-center mt-2 mb-2">
					<ScaleLoader width={7} height={50} color={"#001563"} />
				</div>
			)}
			{prod && prd ? (
				prod && prd && prd.length > 3 ? (
					<p
						className="text-center"
						onClick={() =>
							this.props.history.push({
								pathname: "/viewcart",
								state: { data: prd },
							})
						}
					>
						View All
					</p>
				) : null
			) : null}
		</div>

		<div className="mini-cart__inner">
			<div className="mini-cart__recap">
				{/* <div className="mini-cart__recap-price-line">
																			<span>Total</span>
																			<span>₹119.95</span>
																		</div> */}
				<div className="mini-cart__button-container">
					<div className="button-group button-group--loose button-group--fit">
						<p
							style={{ color: "white" }}
							onClick={() =>
								this.props.history.push({
									pathname: "/viewcart",
									state: { data: prd },
								})
							}
							className="button button--secondary"
						>
							View cart
						</p>
						<button
							type="submit"
							name="checkout"
							onClick={this.checkoutpage}
							className="button button--primary"
						>
							Checkout
						</button>
					</div>
				</div>
			</div>
		</div>
	</DropdownItem>
</DropdownMenu>;
