import React from "react";

const Input = ({ name, placeholder, error, ...rest }) => {
  return (
    <div>
      <input
        {...rest}
        name={name}
        id={name}
        placeholder={placeholder}
        className="form-control input-lg br-tr-md-0 br-br-md-0 my-5"
        multiple
      />
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Input;
